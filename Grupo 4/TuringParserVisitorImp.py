import llvmlite.ir as ir
import llvmlite.binding as llvm
from ctypes import CFUNCTYPE, c_int
from TuringParserVisitor import TuringParserVisitor
from TuringParser import TuringParser

class TuringParserVisitorImp(TuringParserVisitor):

    def __init__(self):
            # Inicializar LLVM
        llvm.initialize()
        llvm.initialize_native_target()
        llvm.initialize_native_asmprinter()

        self.symbol_table_2 = {}
        self.input_array = []

        self.module = ir.Module(name="TuringModule")
        self.module.triple = llvm.get_default_triple()

        self.input = None
        self.blank_space = None
        self.start_state = None
        self.current_state = None
        self.head = 1


        self.builder = None
        self.numberStates = 0
        self.symbol_table = {}
        self.dot_graph=[]
        self.currentStateVisit=None

    def __del__(self):
        # Finalizar LLVM
        llvm.shutdown()

    def visitProgram(self, ctx:TuringParser.ProgramContext):
        return self.visitChildren(ctx)


    def visitDeclare(self, ctx:TuringParser.DeclareContext):
        declaration_type = ctx.getChild(0).getText()
        value = ctx.getChild(2).getText()

        if not self.builder:
            # Creamos una funcion para manejar las asignaciones dentro de un basic block
            func_type = ir.FunctionType(ir.VoidType(), [])
            func = ir.Function(self.module, func_type, name="main")
            entry_block = func.append_basic_block("asignations")
            self.builder = ir.IRBuilder(entry_block)
        
        if declaration_type == "blank":

            blank_value = value
            self.blank_space = blank_value[0]
            
            # Crear una variable en IR para blank_space
            blank_var = ir.GlobalVariable(self.module, ir.IntType(8), "blank_space")
            blank_var.linkage = 'common'
            blank_var.initializer = ir.Constant(ir.IntType(8),  ord(blank_value[0]))

            # Añadimos a la tabla de simbolos
            self.symbol_table["blank_space"] = blank_var

            # Crear una variable en el basic block para almacenar el valor
            blank_ptr = self.builder.alloca(ir.IntType(8), name="blank_ptr")
            blank_const = ir.Constant(ir.IntType(8), ord(blank_value[0]))
            self.builder.store(blank_const, blank_ptr)
            
            
        elif declaration_type == "input":

            # Asignamos los valores
            input_value = value
            self.input = input_value
            input_length = len(input_value) + 2
            for i in range( input_length):
                if i == 0 or i == input_length - 1:
                    self.input_array.append(self.blank_space)
                else:
                    self.input_array.append(self.input[i-1])

            # Crear el arreglo en IR para la cinta de la maquina de turing
            tape_array = ir.GlobalVariable(self.module, ir.ArrayType(ir.IntType(8), input_length), name="input")
            tape_array.linkage = 'common'

            # Inicializar la cinta con los valores del input y blank_space
            tape_init = [self.symbol_table["blank_space"].initializer.constant]
            tape_init.extend(map(ord, input_value))
            tape_init.append(self.symbol_table["blank_space"].initializer.constant)

            # Crear la constante inicializadora
            tape_const = ir.Constant(ir.ArrayType(ir.IntType(8), input_length), tape_init)
            tape_array.initializer = tape_const

            # Añadir a la tabla de símbolos
            self.symbol_table["input"] = tape_array

            # Crear una variable en el basic block para almacenar el arreglo
            tape_ptr = self.builder.alloca(ir.ArrayType(ir.IntType(8), input_length), name="input_ptr")
            self.builder.store(tape_const, tape_ptr)

        
        elif declaration_type == "start_state":

            # Asignamos los valores
            start_state_value = value
            self.start_state = start_state_value
            self.current_state = start_state_value
            # Crear un arreglo global en LLVM para start_state
            start_state_length = len(start_state_value) + 1  # Longitud del string + terminador nulo
            start_state_array_type = ir.ArrayType(ir.IntType(8), start_state_length)
            start_state_array = ir.GlobalVariable(self.module, start_state_array_type, "start_state")
            start_state_array.linkage = 'common'

            # Inicializar el arreglo con el string y el terminador nulo
            start_state_init = [ord(char) for char in start_state_value] + [0]
            start_state_const = ir.Constant(start_state_array_type, start_state_init)
            start_state_array.initializer = start_state_const

            # Añadir a la tabla de símbolos
            self.symbol_table["start_state"] = start_state_array

            # Crear un puntero en el basic block para almacenar el arreglo
            start_state_ptr = self.builder.alloca(start_state_array_type, name="start_state_ptr")
            self.builder.store(start_state_const, start_state_ptr)

            # Crear una variable global para current_state
            current_state_array_type = ir.ArrayType(ir.IntType(8), start_state_length)
            current_state_array = ir.GlobalVariable(self.module, current_state_array_type, "current_state")
            current_state_array.linkage = 'common'
            current_state_array.initializer = start_state_const

            # Añadir a la tabla de símbolos
            self.symbol_table["current_state"] = current_state_array

            # Crear un puntero en el basic block para almacenar el arreglo
            current_state_ptr = self.builder.alloca(current_state_array_type, name="current_state_ptr")
            self.builder.store(start_state_const, current_state_ptr)

             # Crear una variable global para head (cabecilla)
            head_var = ir.GlobalVariable(self.module, ir.IntType(32), "head")
            head_var.linkage = 'common'
            head_var.initializer = ir.Constant(ir.IntType(32), 1)

            # Añadir a la tabla de símbolos
            self.symbol_table["head"] = head_var

            # Crear un puntero en el basic block para almacenar la cabecilla
            head_ptr = self.builder.alloca(ir.IntType(32), name="head_ptr")
            self.builder.store(ir.Constant(ir.IntType(32), 0), head_ptr)

        # Verificar si se han declarado todas las variables antes de cerrar el basic block
        if "blank_space" in self.symbol_table and "input" in self.symbol_table and "start_state" in self.symbol_table:
            self.builder.ret_void()
    
        return None


    def visitElem(self, ctx:TuringParser.ElemContext):
        return self.visitChildren(ctx)


    def visitInstruct(self, ctx:TuringParser.InstructContext):
        return self.visitChildren(ctx)


    def visitWriteExpr(self, ctx:TuringParser.WriteExprContext):
        return self.visitChildren(ctx)


    def visitMoveExpr(self, ctx:TuringParser.MoveExprContext):
        return self.visitChildren(ctx)


    def visitTable(self, ctx:TuringParser.TableContext):
        
        # Crear variables para almacenar las transiciones de la tabla
        transitions_list = []

        # Recorrer cada estado y sus transiciones
        for table_state_ctx in ctx.tableState():
            state_name = table_state_ctx.getChild(0).getText()
            self.currentStateVisit=table_state_ctx.getChild(0).getText()

            if state_name == "done":
                transitions_list.append((state_name, None))
                break
            else:
                state_transitions = self.visitTableState(table_state_ctx)
                transitions_list.append((state_name, state_transitions))
                self.symbol_table_2[state_name]=state_transitions

        # Creamos el tipo de estructura para una transicion
        transition_struct_type = ir.LiteralStructType([
            ir.IntType(8),   # Estado actual (primer caracter del estado, para simplificación)
            ir.IntType(8),   # Condición
            ir.IntType(8),   # Qué escribir en la cinta
            ir.IntType(8),   # Dirección de movimiento
            ir.IntType(8)    # Estado siguiente
        ])
    
        # Crear el tipo de estructura para el array de transiciones
        transition_array_type = ir.ArrayType(transition_struct_type, sum(len(t[1]) if t[1] is not None else 0 for t in transitions_list))
        self.numberStates = sum(len(t[1]) if t[1] is not None else 0 for t in transitions_list)

        # Crear la función que devolverá la tabla de transiciones
        func_type = ir.FunctionType(transition_array_type, [])
        func = ir.Function(self.module, func_type, name="getTransitionTable")
        entry_block = func.append_basic_block("entry")
        builder = ir.IRBuilder(entry_block)

        # Inicializar el array de transiciones
        init_list = []
        for state_name, state_transitions in transitions_list:
            if state_transitions is None:
                #init_list.append(ir.Constant(transition_struct_type, [ir.Constant(ir.IntType(8), 0)] * 4))  # Representar None como 0 en IR
                continue 
            else:
                for condition, (write, move, next_state) in state_transitions:
                    state_name_value = ord(state_name[0])
                    condition_value = ord(condition)
                    write_value = ord(write) if write is not None else -1
                    move_value = ord(move) if move is not None else -1
                    next_state_value = ord(next_state[0]) if next_state is not None else -1
                    init_list.append(ir.Constant(transition_struct_type, [
                        ir.Constant(ir.IntType(8), state_name_value),
                        ir.Constant(ir.IntType(8), condition_value),
                        ir.Constant(ir.IntType(8), write_value),
                        ir.Constant(ir.IntType(8), move_value),
                        ir.Constant(ir.IntType(8), next_state_value)
                    ]))

        transition_array_constant = ir.Constant(transition_array_type, init_list)

        # Retornar el array de transiciones al final de la función
        builder.ret(transition_array_constant)

        # Añadir la función a la tabla de símbolos
        self.symbol_table["getTransitionTable"] = func

        return None


    def visitTableState(self, ctx:TuringParser.TableStateContext):

        state_transitions = []

        for condition_ctx in ctx.condition():
            condition = self.visitCondition(condition_ctx)
            state_transitions.append(condition)

        return state_transitions


    def visitCondition(self, ctx:TuringParser.ConditionContext):
        
        condition_value = ctx.getChild(0).getText()
        
        # Verificar si hay instrucciones y parsearlas si existen
        write_value = None
        move_direction = None
        next_state = None

        if ctx.getChild(2).getChild(1).getText().startswith("write"):
            write_value = ctx.getChild(2).getChild(1).getChild(2).getText()
            if len(ctx.getChild(2).getChild(3).getText()) == 1:
                move_direction = ctx.getChild(2).getChild(3).getText()
            else:
                move_direction = ctx.getChild(2).getChild(3).getChild(0).getText()
                next_state = ctx.getChild(2).getChild(3).getChild(2).getText()
        else:
            if len(ctx.getChild(2).getChild(1).getText()) == 1:
                move_direction = ctx.getChild(2).getChild(1).getText()
            else:
                move_direction = ctx.getChild(2).getChild(1).getChild(0).getText()
                next_state = ctx.getChild(2).getChild(1).getChild(2).getText()

        dotnext = self.currentStateVisit if next_state == None else next_state

        transition = f'"{self.currentStateVisit}" -> "{dotnext}" [label="{condition_value}({write_value},{move_direction})"]'
        self.dot_graph.append(transition)

        return (condition_value, (write_value, move_direction, next_state))
    
    def run(self):

        while self.current_state != "done":
            # Obtener el estado actual de la cinta
            current_symbol = self.input_array[self.head]

            # Obtener las transiciones para el estado actual
            transitions = self.symbol_table_2.get(self.current_state, [])

            # Buscar la transición correspondiente al símbolo actual
            for transition in transitions:
                if current_symbol == transition[0]:
                    # Ejecutar la instrucción de la transición
                    if transition[1][0] is not None:
                        self.input_array[self.head] = transition[1][0]

                    if transition[1][1] == "R":
                        self.head += 1
                    elif transition[1][1] == "L":
                        self.head -= 1

                    if transition[1][2] is not None:
                        self.current_state = transition[1][2]

                    break  # Salir del bucle una vez que se ejecuta la transición adecuada

    # Imprimir el contenido final de la cinta cuando el estado es "done"
        print("".join(self.input_array))

        
    def generate_dot(self):
        self.dot_graph = list(set(self.dot_graph))
        dot_output = 'digraph G {\n'
        for transition in self.dot_graph:
            dot_output += f'    {transition}\n'
        dot_output += '}'
        return dot_output

    def configure_jit_execution(self):
        try:
            # Configurar el motor de ejecución JIT
            target = llvm.Target.from_default_triple()
            target_machine = target.create_target_machine()
            backing_mod = llvm.parse_assembly(str(self.module))
            engine = llvm.create_mcjit_compiler(backing_mod, target_machine)
            backing_mod.verify()
            engine.finalize_object()
            engine.run_static_constructors()
            return engine
        except Exception as e:
            print(f"Error configuring JIT execution: {e}")
            return None



    def generate_ir(self):
        return str(self.module)