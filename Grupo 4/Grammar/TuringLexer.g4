lexer grammar TuringLexer;

LBRACKET: '[' ;
RBRACKET: ']' ;
LCURL   : '{' ;
RCURL   : '}' ;
LPAREN  : '(' ;
RPAREN  : ')' ;
COMMA   : ',' ;
DPOINT  : ':' ;

TABLE   : 'table' ;
WRITE   : 'write' ;
READ    : 'read' ;
RIGHT   : 'R' ;
LEFT    : 'L' ;

INPUT   : 'input' ;
BLANK   : 'blank' ;
START_STATE: 'start_state' ;

ID      : [a-zA-Z_][a-zA-Z0-9_]* ;
NUMBER  : [0-1]+ ;

COMMENT : '#'(.)*?[\n] -> skip ;
WS      : [ \t\n\r]+ -> skip ;