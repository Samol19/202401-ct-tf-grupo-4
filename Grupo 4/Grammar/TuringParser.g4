parser grammar TuringParser;
options { tokenVocab = TuringLexer; }

program
    : (declare | table)+
    ;

declare
    : (INPUT | BLANK | START_STATE) DPOINT (ID | NUMBER)
    ;

elem
    : ID
    | NUMBER
    | (RIGHT | LEFT)
    ;

instruct
    : LPAREN writeExpr COMMA moveExpr RPAREN
    | LPAREN writeExpr RPAREN
    | LPAREN moveExpr RPAREN
    ;

writeExpr
    : WRITE DPOINT elem
    ;

moveExpr
    : (RIGHT | LEFT)
    | (RIGHT | LEFT) DPOINT ID    
    ;

table
    : TABLE LCURL tableState* RCURL
    ;

tableState
    : ID DPOINT condition*
    ;

condition
    : elem DPOINT instruct
    ;

