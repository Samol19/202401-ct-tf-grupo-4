; ModuleID = "TuringModule"
target triple = "x86_64-pc-windows-msvc"
target datalayout = ""

define void @"main"()
{
asignations:
  %"blank_ptr" = alloca i8
  store i8 98, i8* %"blank_ptr"
  %"input_ptr" = alloca [7 x i8]
  store [7 x i8] [i8 98, i8 49, i8 48, i8 49, i8 48, i8 49, i8 98], [7 x i8]* %"input_ptr"
  %"start_state_ptr" = alloca [6 x i8]
  store [6 x i8] [i8 114, i8 105, i8 103, i8 104, i8 116, i8 0], [6 x i8]* %"start_state_ptr"
  %"current_state_ptr" = alloca [6 x i8]
  store [6 x i8] [i8 114, i8 105, i8 103, i8 104, i8 116, i8 0], [6 x i8]* %"current_state_ptr"
  %"head_ptr" = alloca i32
  store i32 0, i32* %"head_ptr"
  ret void
}

@"blank_space" = common global i8 98
@"input" = common global [7 x i8] [i8 98, i8 49, i8 48, i8 49, i8 48, i8 49, i8 98]
@"start_state" = common global [6 x i8] [i8 114, i8 105, i8 103, i8 104, i8 116, i8 0]
@"current_state" = common global [6 x i8] [i8 114, i8 105, i8 103, i8 104, i8 116, i8 0]
@"head" = common global i32 1
define [6 x {i8, i8, i8, i8, i8}] @"getTransitionTable"()
{
entry:
  ret [6 x {i8, i8, i8, i8, i8}] [{i8, i8, i8, i8, i8} {i8 114, i8 49, i8 -1, i8 82, i8 -1}, {i8, i8, i8, i8, i8} {i8 114, i8 48, i8 -1, i8 82, i8 -1}, {i8, i8, i8, i8, i8} {i8 114, i8 98, i8 -1, i8 76, i8 99}, {i8, i8, i8, i8, i8} {i8 99, i8 49, i8 48, i8 76, i8 -1}, {i8, i8, i8, i8, i8} {i8 99, i8 48, i8 49, i8 76, i8 100}, {i8, i8, i8, i8, i8} {i8 99, i8 98, i8 49, i8 76, i8 100}]
}
