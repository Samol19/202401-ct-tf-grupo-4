# Generated from TuringParser.g4 by ANTLR 4.13.1
from antlr4 import *
if "." in __name__:
    from .TuringParser import TuringParser
else:
    from TuringParser import TuringParser

# This class defines a complete generic visitor for a parse tree produced by TuringParser.

class TuringParserVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by TuringParser#program.
    def visitProgram(self, ctx:TuringParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#declare.
    def visitDeclare(self, ctx:TuringParser.DeclareContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#elem.
    def visitElem(self, ctx:TuringParser.ElemContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#instruct.
    def visitInstruct(self, ctx:TuringParser.InstructContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#writeExpr.
    def visitWriteExpr(self, ctx:TuringParser.WriteExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#moveExpr.
    def visitMoveExpr(self, ctx:TuringParser.MoveExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#table.
    def visitTable(self, ctx:TuringParser.TableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#tableState.
    def visitTableState(self, ctx:TuringParser.TableStateContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringParser#condition.
    def visitCondition(self, ctx:TuringParser.ConditionContext):
        return self.visitChildren(ctx)



del TuringParser