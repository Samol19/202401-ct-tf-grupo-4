import sys
import os
import subprocess

from antlr4 import *

from TuringLexer import TuringLexer
from TuringParser import TuringParser
from TuringParserVisitorImp import TuringParserVisitorImp

from llvmlite import ir
import llvmlite.ir as ir
import llvmlite.binding as llvm

def process_input(input_text):
    input_stream = InputStream(input_text)
    lexer = TuringLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = TuringParser(stream)
    tree = parser.program()

    visitor = TuringParserVisitorImp()
    visitor.visit(tree)
    
    llvm_ir = visitor.generate_ir()
    #print(llvm_ir)

    # Generar y mostrar el contenido del grafo DOT
    dot_output = visitor.generate_dot()
    # Guardar el contenido en un archivo de texto
    with open("grafo.dot", "w") as file:
        file.write(dot_output)  

    #guardar IR en formato .ll
    with open("output.ll", "w") as file:
        file.write(llvm_ir)

    try:
        with open("optimized.ll", "w") as file:
            file.write(' ')
        # Nombre del archivo con el código LLVM IR
        llvm_file = "output.ll"
        # Nombre del archivo de salida optimizado
        optimized_file = "optimized.ll"
        # Lista de optimizaciones a aplicar (inlining/opt. global de variables y func. /eliminar codigo muerto / combinar funciones )
        optimizations = ["-inline -dce -globalopt -instcombine -globaldce -deadinst"]
        # Comando para ejecutar opt con las optimizaciones deseadas
        command = ["opt", llvm_file] + optimizations + ["-o", optimized_file]
        # Ejecutar el comando
        subprocess.run(command, check=True)
        print(f"Optimización completada. Archivo optimizado guardado en '{optimized_file}'.")
    #si no se encuentra opt en funcion de comandos no optimizar    
    except:
        pass
    
    visitor.run()

    #EJECUCION DEL JIT
    visitor.configure_jit_execution()


def main():

    print("Welcome to the Turing Machine Compiler. Type 'RUN' on a new line to finish input.")
    print("Type 'CLS' to clear the screen and 'EXIT' to exit the program.")
    while True:
        input_lines = []
        while True:
            line = input(">> ")
            if line.strip().upper() == "RUN":
                break
            elif line.strip().upper() == "EXIT":
                sys.exit(0)
            elif line.strip().upper() == "CLS":
                os.system('cls' if os.name == 'nt' else 'clear')
                print("Screen cleared. You can continue entering your input.")
                input_lines = []  # Reset input lines after clearing the screen
                continue
            input_lines.append(line)
        input_text = "\n".join(input_lines)
        try:
            process_input(input_text)
        except Exception as e:
            print(f"Error: {e}")


if __name__ == '__main__':
    main()