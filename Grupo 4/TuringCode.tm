blank: _
input: 1100100
start_state: derecha

table {
  derecha:
    1     : (R)
    0     : (R)
    _    : (L: regreso)
  regreso:
    1      : (write: 0, L)
    0      : (write: 1, L: done)
    _     : (write: 1, L: done)
  done:
}

