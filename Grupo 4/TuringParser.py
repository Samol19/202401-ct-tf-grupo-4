# Generated from TuringParser.g4 by ANTLR 4.13.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,20,82,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,1,0,1,0,4,0,21,8,0,11,0,12,0,22,1,1,1,1,1,1,1,
        1,1,2,1,2,1,2,3,2,32,8,2,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,
        1,3,1,3,1,3,1,3,3,3,48,8,3,1,4,1,4,1,4,1,4,1,5,1,5,1,5,1,5,3,5,58,
        8,5,1,6,1,6,1,6,5,6,63,8,6,10,6,12,6,66,9,6,1,6,1,6,1,7,1,7,1,7,
        5,7,73,8,7,10,7,12,7,76,9,7,1,8,1,8,1,8,1,8,1,8,0,0,9,0,2,4,6,8,
        10,12,14,16,0,3,1,0,14,16,1,0,17,18,1,0,12,13,81,0,20,1,0,0,0,2,
        24,1,0,0,0,4,31,1,0,0,0,6,47,1,0,0,0,8,49,1,0,0,0,10,57,1,0,0,0,
        12,59,1,0,0,0,14,69,1,0,0,0,16,77,1,0,0,0,18,21,3,2,1,0,19,21,3,
        12,6,0,20,18,1,0,0,0,20,19,1,0,0,0,21,22,1,0,0,0,22,20,1,0,0,0,22,
        23,1,0,0,0,23,1,1,0,0,0,24,25,7,0,0,0,25,26,5,8,0,0,26,27,7,1,0,
        0,27,3,1,0,0,0,28,32,5,17,0,0,29,32,5,18,0,0,30,32,7,2,0,0,31,28,
        1,0,0,0,31,29,1,0,0,0,31,30,1,0,0,0,32,5,1,0,0,0,33,34,5,5,0,0,34,
        35,3,8,4,0,35,36,5,7,0,0,36,37,3,10,5,0,37,38,5,6,0,0,38,48,1,0,
        0,0,39,40,5,5,0,0,40,41,3,8,4,0,41,42,5,6,0,0,42,48,1,0,0,0,43,44,
        5,5,0,0,44,45,3,10,5,0,45,46,5,6,0,0,46,48,1,0,0,0,47,33,1,0,0,0,
        47,39,1,0,0,0,47,43,1,0,0,0,48,7,1,0,0,0,49,50,5,10,0,0,50,51,5,
        8,0,0,51,52,3,4,2,0,52,9,1,0,0,0,53,58,7,2,0,0,54,55,7,2,0,0,55,
        56,5,8,0,0,56,58,5,17,0,0,57,53,1,0,0,0,57,54,1,0,0,0,58,11,1,0,
        0,0,59,60,5,9,0,0,60,64,5,3,0,0,61,63,3,14,7,0,62,61,1,0,0,0,63,
        66,1,0,0,0,64,62,1,0,0,0,64,65,1,0,0,0,65,67,1,0,0,0,66,64,1,0,0,
        0,67,68,5,4,0,0,68,13,1,0,0,0,69,70,5,17,0,0,70,74,5,8,0,0,71,73,
        3,16,8,0,72,71,1,0,0,0,73,76,1,0,0,0,74,72,1,0,0,0,74,75,1,0,0,0,
        75,15,1,0,0,0,76,74,1,0,0,0,77,78,3,4,2,0,78,79,5,8,0,0,79,80,3,
        6,3,0,80,17,1,0,0,0,7,20,22,31,47,57,64,74
    ]

class TuringParser ( Parser ):

    grammarFileName = "TuringParser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'['", "']'", "'{'", "'}'", "'('", "')'", 
                     "','", "':'", "'table'", "'write'", "'read'", "'R'", 
                     "'L'", "'input'", "'blank'", "'start_state'" ]

    symbolicNames = [ "<INVALID>", "LBRACKET", "RBRACKET", "LCURL", "RCURL", 
                      "LPAREN", "RPAREN", "COMMA", "DPOINT", "TABLE", "WRITE", 
                      "READ", "RIGHT", "LEFT", "INPUT", "BLANK", "START_STATE", 
                      "ID", "NUMBER", "COMMENT", "WS" ]

    RULE_program = 0
    RULE_declare = 1
    RULE_elem = 2
    RULE_instruct = 3
    RULE_writeExpr = 4
    RULE_moveExpr = 5
    RULE_table = 6
    RULE_tableState = 7
    RULE_condition = 8

    ruleNames =  [ "program", "declare", "elem", "instruct", "writeExpr", 
                   "moveExpr", "table", "tableState", "condition" ]

    EOF = Token.EOF
    LBRACKET=1
    RBRACKET=2
    LCURL=3
    RCURL=4
    LPAREN=5
    RPAREN=6
    COMMA=7
    DPOINT=8
    TABLE=9
    WRITE=10
    READ=11
    RIGHT=12
    LEFT=13
    INPUT=14
    BLANK=15
    START_STATE=16
    ID=17
    NUMBER=18
    COMMENT=19
    WS=20

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declare(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TuringParser.DeclareContext)
            else:
                return self.getTypedRuleContext(TuringParser.DeclareContext,i)


        def table(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TuringParser.TableContext)
            else:
                return self.getTypedRuleContext(TuringParser.TableContext,i)


        def getRuleIndex(self):
            return TuringParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = TuringParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 20 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 20
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [14, 15, 16]:
                    self.state = 18
                    self.declare()
                    pass
                elif token in [9]:
                    self.state = 19
                    self.table()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 22 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & 115200) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclareContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DPOINT(self):
            return self.getToken(TuringParser.DPOINT, 0)

        def INPUT(self):
            return self.getToken(TuringParser.INPUT, 0)

        def BLANK(self):
            return self.getToken(TuringParser.BLANK, 0)

        def START_STATE(self):
            return self.getToken(TuringParser.START_STATE, 0)

        def ID(self):
            return self.getToken(TuringParser.ID, 0)

        def NUMBER(self):
            return self.getToken(TuringParser.NUMBER, 0)

        def getRuleIndex(self):
            return TuringParser.RULE_declare

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclare" ):
                return visitor.visitDeclare(self)
            else:
                return visitor.visitChildren(self)




    def declare(self):

        localctx = TuringParser.DeclareContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_declare)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 114688) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 25
            self.match(TuringParser.DPOINT)
            self.state = 26
            _la = self._input.LA(1)
            if not(_la==17 or _la==18):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElemContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TuringParser.ID, 0)

        def NUMBER(self):
            return self.getToken(TuringParser.NUMBER, 0)

        def RIGHT(self):
            return self.getToken(TuringParser.RIGHT, 0)

        def LEFT(self):
            return self.getToken(TuringParser.LEFT, 0)

        def getRuleIndex(self):
            return TuringParser.RULE_elem

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitElem" ):
                return visitor.visitElem(self)
            else:
                return visitor.visitChildren(self)




    def elem(self):

        localctx = TuringParser.ElemContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_elem)
        self._la = 0 # Token type
        try:
            self.state = 31
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [17]:
                self.enterOuterAlt(localctx, 1)
                self.state = 28
                self.match(TuringParser.ID)
                pass
            elif token in [18]:
                self.enterOuterAlt(localctx, 2)
                self.state = 29
                self.match(TuringParser.NUMBER)
                pass
            elif token in [12, 13]:
                self.enterOuterAlt(localctx, 3)
                self.state = 30
                _la = self._input.LA(1)
                if not(_la==12 or _la==13):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InstructContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(TuringParser.LPAREN, 0)

        def writeExpr(self):
            return self.getTypedRuleContext(TuringParser.WriteExprContext,0)


        def COMMA(self):
            return self.getToken(TuringParser.COMMA, 0)

        def moveExpr(self):
            return self.getTypedRuleContext(TuringParser.MoveExprContext,0)


        def RPAREN(self):
            return self.getToken(TuringParser.RPAREN, 0)

        def getRuleIndex(self):
            return TuringParser.RULE_instruct

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstruct" ):
                return visitor.visitInstruct(self)
            else:
                return visitor.visitChildren(self)




    def instruct(self):

        localctx = TuringParser.InstructContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_instruct)
        try:
            self.state = 47
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 33
                self.match(TuringParser.LPAREN)
                self.state = 34
                self.writeExpr()
                self.state = 35
                self.match(TuringParser.COMMA)
                self.state = 36
                self.moveExpr()
                self.state = 37
                self.match(TuringParser.RPAREN)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 39
                self.match(TuringParser.LPAREN)
                self.state = 40
                self.writeExpr()
                self.state = 41
                self.match(TuringParser.RPAREN)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 43
                self.match(TuringParser.LPAREN)
                self.state = 44
                self.moveExpr()
                self.state = 45
                self.match(TuringParser.RPAREN)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WriteExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WRITE(self):
            return self.getToken(TuringParser.WRITE, 0)

        def DPOINT(self):
            return self.getToken(TuringParser.DPOINT, 0)

        def elem(self):
            return self.getTypedRuleContext(TuringParser.ElemContext,0)


        def getRuleIndex(self):
            return TuringParser.RULE_writeExpr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWriteExpr" ):
                return visitor.visitWriteExpr(self)
            else:
                return visitor.visitChildren(self)




    def writeExpr(self):

        localctx = TuringParser.WriteExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_writeExpr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 49
            self.match(TuringParser.WRITE)
            self.state = 50
            self.match(TuringParser.DPOINT)
            self.state = 51
            self.elem()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MoveExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RIGHT(self):
            return self.getToken(TuringParser.RIGHT, 0)

        def LEFT(self):
            return self.getToken(TuringParser.LEFT, 0)

        def DPOINT(self):
            return self.getToken(TuringParser.DPOINT, 0)

        def ID(self):
            return self.getToken(TuringParser.ID, 0)

        def getRuleIndex(self):
            return TuringParser.RULE_moveExpr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMoveExpr" ):
                return visitor.visitMoveExpr(self)
            else:
                return visitor.visitChildren(self)




    def moveExpr(self):

        localctx = TuringParser.MoveExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_moveExpr)
        self._la = 0 # Token type
        try:
            self.state = 57
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 53
                _la = self._input.LA(1)
                if not(_la==12 or _la==13):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 54
                _la = self._input.LA(1)
                if not(_la==12 or _la==13):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 55
                self.match(TuringParser.DPOINT)
                self.state = 56
                self.match(TuringParser.ID)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TableContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TABLE(self):
            return self.getToken(TuringParser.TABLE, 0)

        def LCURL(self):
            return self.getToken(TuringParser.LCURL, 0)

        def RCURL(self):
            return self.getToken(TuringParser.RCURL, 0)

        def tableState(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TuringParser.TableStateContext)
            else:
                return self.getTypedRuleContext(TuringParser.TableStateContext,i)


        def getRuleIndex(self):
            return TuringParser.RULE_table

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTable" ):
                return visitor.visitTable(self)
            else:
                return visitor.visitChildren(self)




    def table(self):

        localctx = TuringParser.TableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_table)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            self.match(TuringParser.TABLE)
            self.state = 60
            self.match(TuringParser.LCURL)
            self.state = 64
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==17:
                self.state = 61
                self.tableState()
                self.state = 66
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 67
            self.match(TuringParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TableStateContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TuringParser.ID, 0)

        def DPOINT(self):
            return self.getToken(TuringParser.DPOINT, 0)

        def condition(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TuringParser.ConditionContext)
            else:
                return self.getTypedRuleContext(TuringParser.ConditionContext,i)


        def getRuleIndex(self):
            return TuringParser.RULE_tableState

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTableState" ):
                return visitor.visitTableState(self)
            else:
                return visitor.visitChildren(self)




    def tableState(self):

        localctx = TuringParser.TableStateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_tableState)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(TuringParser.ID)
            self.state = 70
            self.match(TuringParser.DPOINT)
            self.state = 74
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 71
                    self.condition() 
                self.state = 76
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def elem(self):
            return self.getTypedRuleContext(TuringParser.ElemContext,0)


        def DPOINT(self):
            return self.getToken(TuringParser.DPOINT, 0)

        def instruct(self):
            return self.getTypedRuleContext(TuringParser.InstructContext,0)


        def getRuleIndex(self):
            return TuringParser.RULE_condition

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCondition" ):
                return visitor.visitCondition(self)
            else:
                return visitor.visitChildren(self)




    def condition(self):

        localctx = TuringParser.ConditionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_condition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 77
            self.elem()
            self.state = 78
            self.match(TuringParser.DPOINT)
            self.state = 79
            self.instruct()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





