# INFORME DE TRABAJO FINAL

## Teoría de Compiladores
### Carrera de Ciencias de la Computación

**Sección:** CC61

**Alumnos:**
- U202116032 Pedro Alejandro Retuerto Diaz
- U202122837 Fernando Samuel Paredes Espinoza
- U202114233 Anthony Hans Tarrillo Ayllón
- U202121973 Andres Joshua Rodriguez Guerrero

Junio 2024

---

## CONTENIDO

- Introducción
- Objetivos
- Marco Teórico
- Metodología
- Conclusiones
- Referencias

Link del video de demostración: https://youtu.be/tTp5Xfnt_nY

Link del PPT: https://www.canva.com/design/DAGJbJ2ccjs/5aCcamit6sNdP7j8OnOx9A/edit?utm_content=DAGJbJ2ccjs&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton

Informe en word: https://docs.google.com/document/d/1YLL35Clbe4WXaW5cTP7KMSOnQYVjMcYwrpNMoc8RCsQ/edit


---

## Introducción

En el presente trabajo se busca abordar la necesidad de una herramienta eficiente para representar y simular máquinas de Turing. Una máquina de Turing es un dispositivo que manipula símbolos sobre una tira de cinta de acuerdo con una tabla de reglas, la cual es destinada a simular algoritmos informáticos. La principal motivación detrás de este proyecto es proporcionar una solución integral que facilite la representación, simulación y visualización de los resultados de la cinta de las máquinas de Turing. Esto incluye la definición de un lenguaje de representación estándar, la implementación de un simulador que pueda ejecutar de manera eficiente (utilizando técnicas JIT), y la creación de visualizaciones gráficas que permitan entender el funcionamiento de estas máquinas. Para lograr la solución de la problemática se han planteado las siguientes propuestas:
- La definición de un lenguaje para representar una máquina de Turing.
- La implementación de un simulador que ejecute la máquina de Turing y muestre el estado final de la cinta.
- La incorporación de una ejecución Just-In-Time (JIT) para mejorar la eficiencia.
- La generación de una representación gráfica de la máquina de Turing utilizando el lenguaje DOT de Graphviz.

---

## Objetivos

- **Definir un Lenguaje de Representación:** Crear un lenguaje estándar y claro que permita la representación de cualquier máquina de Turing.
- **Implementar un Simulador de Máquina de Turing:** Desarrollar un programa que, a partir de los datos de la máquina, ejecute sus operaciones e imprima el estado final de la cinta.
- **Generar Visualizaciones en Graphviz:** Producir una representación gráfica de la máquina de Turing en formato DOT, que permita visualizar la máquina como un grafo.

---

## Marco Teórico

### Máquinas de Turing

Las máquinas de Turing fueron propuestas por Alan Turing en 1936 como un modelo matemático para describir la manipulación de símbolos en una cinta infinita según un conjunto de reglas. Una máquina de Turing consiste en:
- **Cinta:** Un soporte de almacenamiento que se puede considerar infinito en ambas direcciones y dividido en celdas.
- **Cabezal de Lectura/Escritura:** Que puede moverse a la izquierda o a la derecha en la cinta y leer o escribir símbolos.
- **Conjunto de Estados:** Incluye un estado inicial y uno o más estados de aceptación o rechazo.
- **Tabla de Transiciones:** Define las reglas de cómo la máquina cambia de estado basado en el símbolo leído y escribe en la cinta.

### JIT (Just-In-Time)

La ejecución JIT es una técnica que combina la interpretación y la compilación en tiempo de ejecución. En lugar de interpretar cada instrucción una por una, la ejecución JIT traduce bloques de instrucciones en código máquina nativo justo antes de su ejecución, mejorando la eficiencia.

---

## Metodología

### ANTLR4

- Se eligió ANTLR4 para definir la gramática de nuestro lenguaje de representación de máquinas de Turing debido a su capacidad para generar analizadores léxicos y sintácticos.
- Se diseñó una gramática en ANTLR4 que permite especificar los elementos de una máquina de Turing.
- Usando ANTLR4, generamos un analizador que convierte descripciones de máquinas de Turing en una estructura de datos utilizable por el simulador.

### LLVM

LLVM es un marco robusto para la optimización y generación de código, ampliamente utilizado para la compilación JIT. Se seleccionó LLVM debido a su capacidad para producir código máquina altamente optimizado.

#### Definición del Lenguaje

El lenguaje que se utilizó para definir la máquina de Turing se encuentra en la carpeta Grammar del repositorio. Se hizo uso de ANTLR4 y permite distintas configuraciones posibles para su lectura, tanto como el "input", "espacio_blanco" (' ' dentro de la cinta), distintas nomenclaturas de acciones dentro de la tabla de instrucciones, entre otros.

#### Árbol Sintáctico

Asimismo, se logró compilar los archivos .g4 mediante el uso de herramientas de ANTLR. En este proyecto se decidió hacer uso del lenguaje de Python 3, por ello se usó el siguiente comando:

 -no-listener -visitor -Dlanguage=Python3 * .g4

### Frontend:
Se utilizó Python junto con las clases generadas por ANTLR4 para crear una interfaz de comandos. Esta permite configurar la máquina de Turing, ingresar la cinta inicial y ejecutar la simulación de forma manual o automática.
![alt text](image-4.png)

### Generación de IR:
Se empleó LLVMlite, una biblioteca de Python que facilita operaciones avanzadas con LLVM:

# Compilación Just-In-Time (JIT): Permite compilar código Python a código máquina durante la ejecución para mejorar el rendimiento.
# Generación de Código: Facilita la creación dinámica de código máquina para compiladores e intérpretes.
# Optimización de Código: Se utilizó la herramienta OPT de LLVM para optimizar el código LLVM IR generado, aplicando las siguientes opciones:
![alt text](image-1.png)

-opt -inline -dce -globalopt -instcombine -globaldce -deadinst
Estas opciones incluyen inlining de funciones, eliminación de código muerto, optimización de variables y funciones globales, combinación de instrucciones para mejorar el rendimiento y eliminación de instrucciones inútiles.

### Generación del DOT:
Se logró generar código en formato DOT mediante el recorrido de condiciones en la implementación del visitor.

Ejecución del Gráfico DOT Generado:
![alt text](image.png)

## La generación del ejecutable (JIT) 
Este realiza configurando un entorno de ejecución LLVM dentro del visitor. Este compila el IR LLVM generado en self.module y ejecuta la función main a través de ctypes.
![alt text](image-3.png)
![alt text](image-2.png)


### Conclusiones:
Se desarrolló una herramienta integral para la representación, simulación y visualización de máquinas de Turing:

Definición de Lenguaje: Se creó un lenguaje estándar con ANTLR4 para representar con precisión máquinas de Turing.
Simulador: Implementación eficiente que ejecuta operaciones y utiliza ejecución JIT. La generación de la cinta final está en proceso para mejorar la eficiencia.
Visualización: Se generaron visualizaciones gráficas en formato DOT con Graphviz para facilitar la comprensión visual.
Optimización: Uso de LLVM y llvmlite para optimizar el rendimiento del simulador y la generación de código máquina dinámico.


### Bibliografía:

Bootcamp AI. (sin fecha). Máquinas de Turing. Medium. Recuperado el 28 de junio de 2024, de https://bootcampai.medium.com/m%C3%A1quinas-de-turing-c329ccc270f#:~:text=Una%20m%C3%A1quina%20de%20Turing%20es,la%20memoria%20de%20la%20computadora.

llvmlite. (s.f.). Documentación de llvmlite. Recuperado el 28 de junio de 2024, de https://www.llvm.org/docs/GettingStarted.html

ANTLR. (s.f.). ANTLR: The ANTLR Parser Generator. Recuperado el 28 de junio de 2024, de https://www.antlr.org
